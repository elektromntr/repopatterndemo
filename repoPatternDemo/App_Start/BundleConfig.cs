﻿using System.Web.Optimization;

namespace repoPatternDemo
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-reboot.css",
                "~/Content/bootstrap-grid.css"));
            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/umd/popper.min.js",
                "~/Scripts/umd/popper-utils.js"));

            //BundleTable.EnableOptimizations = true;
        }
    }
}