﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class Gallery
    {
        public int GalleryID { get; set; }
        public string Name { get; set; }
        public int? HeroId { get; set; }
        [ForeignKey("HeroID")]
        public virtual Hero Hero { get; set; }
        public string GalleryPath
        {
            get { return "/gallery" + GalleryID; }
            set { }
        }
    }
}