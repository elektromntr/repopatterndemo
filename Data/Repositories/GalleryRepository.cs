﻿using System.Data.Entity;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public class GalleryRepository : RepositoryBase<Gallery>, IGalleryRepository
    {
        public GalleryRepository(IDbFactory dbFactory) 
            : base(dbFactory) { }
    }

    public Gallery GetGalleryByHeroName(int heroId)
    {
        var gallery = DbContext.Galleries(g => g.HeroId == heroId);
    }

    public interface IGalleryRepository : IRepository<Gallery>
    {

    }
}