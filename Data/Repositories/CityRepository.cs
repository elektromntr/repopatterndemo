﻿using Model;
using System;
using System.Linq;
using Data.Infrastructure;

namespace Data.Repositories
{
    public class CityRepository : RepositoryBase<City>, ICityRepository
    {
        public CityRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public City GetCityByName(string cityName)
        {
            var city = DbContext.Cities.FirstOrDefault(c => c.Name == cityName);

            return city;
        }

        public override void Update(City entity)
        {
            base.Update(entity);
        }
    }

    public interface ICityRepository : IRepository<City>
        {
            City GetCityByName(string cityName);
        }   
}
