﻿using System.Linq;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public class HeroRepository : RepositoryBase<Hero>, IHeroRepository
    {
        public HeroRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


        public Hero GetHeroByName(string heroName)
        {
            var hero = DbContext.Heroes.FirstOrDefault(h => h.Name == heroName);

            return hero;
        }
    }

    public interface IHeroRepository : IRepository<Hero>
    {
        Hero GetHeroByName(string heroName);
    }
    
}
