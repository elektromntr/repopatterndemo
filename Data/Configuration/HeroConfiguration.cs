﻿using Model;
using System.Data.Entity.ModelConfiguration;
using Model.Models;

namespace Data.Configuration
{
    public class HeroConfiguration : EntityTypeConfiguration<Hero>
    {
        public HeroConfiguration()
        {
            ToTable("Heroes");
            Property(h => h.Name).IsRequired().HasMaxLength(99);
            Property(h => h.CityID).IsRequired();
        }       
    }
}
