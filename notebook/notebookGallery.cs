public class Gallery {
    [Key]
    public int GalleryId
    public int? HeroId
    public virtual Hero Hero
    public string GalleryPath
    {
        get
        {
            return "/gallery" +GalleryId;
        }
        set {}
    }

    /* Można tworzyć n galerii przypisanych do konkretnego Bohatera. Utworzenie galerii powoduje powstanie nowego folderu przypisanego do tej konkretnej galerii. 
    Potrzebne jest utworzenie widoku z możliwością dodawania zdjęć do wybranego folderu.
    Na widoku bohatera muszą być partiale, które będą wyświetlały wszystkie galerie, jedna pod drugą. */